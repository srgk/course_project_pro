/*
 * cupport_functions.cpp
 *
 *  Created on: 8 июня 2015
 *      Author: strangeman
 */

#include <cassert>
#include <cstring>
#include <cctype>

#include "support_functions.hpp"

void PrintMessage( Dates::ExtractStatus _status, std::ostream & _messageStream ) {
	switch( _status ) {
	case Dates::BAD_INPUT_FORMAT:
		_messageStream << "\nYou entered date incorrectly.\nPlease, try again: ";
		break;

	case Dates::BAD_DATE:
		_messageStream << "\nYour date is incorrect.\nPlease, try again: ";
		break;

	case Dates::OK:
		break;

	case Dates::TOO_MUCH_NUMBERS:
		_messageStream << "Warning: Used only three first found numbers.\n";
		break;

	case Dates::NOT_ONLY_NUMBERS:
		_messageStream << "Warning: There is redundant letters in your input. Trying to parse date...\n";
		break;

	default:
		assert( ! "You mustn't be here" );
	}
}

void GetEndianFromUser( std::istream & _stream, Dates::DateEndian & _endian, std::ostream & _messageStream ) {
	CharVector::ChVector temp;
	CharVector::Init( temp );

	const char * availableFormats[] = { "autodetection of", "dd.mm.yyyy", "mm.dd.yyyy", "yyyy.mm.dd" };

	int readEndian = Dates::UNDEF;

	_messageStream << "The list of available formats is:\n"
			"0) " << availableFormats[ 0 ] << " format\n"
			"1) " << availableFormats[ 1 ] << "\n"
			"2) " << availableFormats[ 2 ] << "\n"
			"3) " << availableFormats[ 3 ] << "\n"
			"where \".\" is a separator, \"yyyy\" is year, \"mm\" is month and \"dd\" is date\n\n"

			"Which date format you want to use for your input?\n";

	while( _stream && ! _stream.eof() ) {
		_messageStream << "Please, input 0, 1, 2 or 3: ";
		CharVector::Clear( temp );
		CharVector::GetUntil( temp, _stream, "\n" );
		if ( CharVector::HasOnlyDigits( temp.m_pData ) ) {
			//Проверка: изменил ли кто-то порядок значений в Dates::DateEndian
			assert( Dates::UNDEF == -1 );
			assert( Dates::LITTLE == 0 );
			assert( Dates::MIDDLE == 1 );
			assert( Dates::BIG == 2 );

			readEndian = atoi( temp.m_pData );

			// Проверка корректности введенного значения
			if( ( readEndian >= 0 ) && ( readEndian < 4 ) ) {
				// Если значение корректное - запоминаем ожидаемый формат ввода
				_endian = static_cast<Dates::DateEndian>( readEndian - 1);
				// И выходим из цикла
				break;
			}
		}
		_messageStream << "Irroneous value.\n";
	}
	_messageStream << "\nUsing " << availableFormats[ readEndian ] << " format\n";

	CharVector::Destroy( temp );
}

bool GetDateFromUser( std::istream & _stream, Dates::Date & _targetDate, Dates::DateEndian & _endian, std::ostream & _messageStream ) {
	CharVector::ChVector temp;
	CharVector::Init( temp );
	bool isDateGot = false;
	Dates::ExtractStatus extrStatus;

	CharVector::Clear( temp );

	while( true ) {
		CharVector::Clear( temp );

		if( _stream.eof() || _stream.fail() ) {
			_messageStream << "\n\nUnable to read your input\n";
			break;
		}

		CharVector::GetUntil( temp, _stream, "\n" );

		if( strstr( temp.m_pData, "exit" ) )
			break;

		extrStatus = Dates::ExtractDate( temp.m_pData, _targetDate, _endian );

		PrintMessage( extrStatus, _messageStream );

		assert( Dates::OK == 0 );
		assert( Dates::BAD_INPUT_FORMAT < Dates::OK ); 

		if( extrStatus >= Dates::OK ) {
			isDateGot = true;
			break;
		}

	}

	CharVector::Destroy( temp );

	return isDateGot;
}
