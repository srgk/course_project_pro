/*
 * main.cpp
 *
 *  Created on: 5 июня 2015
 *      Author: strangeman
 */

#include "support_functions.hpp"

int main (int _argc, char** _argv) {
	CharVector::ChVector temp;
	CharVector::Init( temp );
	Dates::Date tempDate;
	Dates::DateEndian endian;

	GetEndianFromUser( std::cin, endian, std::cout );

	std::cout << "List of allowed separators is \".\" (dot), \" \" (space), \"-\" (hyphen) and \"/\" (slash). Use one of them in input, don't mix \n";

	std::cout << "\nNow input your date in choosen format or input \"exit\" to exit: ";
	while ( GetDateFromUser( std::cin, tempDate, endian, std::cout ) ) {
		Dates::GetTextDate( tempDate, temp );
		std::cout << "\nYour date in text format is:\n";
		std::cout << temp.m_pData << std::endl;
		std::cout << "\nInput date in numerical format or print \"exit\" to exit: ";
	}
	std::cout << "\nExiting..." << std::endl;

	CharVector::Destroy( temp );
	return 0;
}
