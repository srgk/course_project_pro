/*
 * numerals.hpp
 *
 *  Created on: 7 июня 2015
 *      Author: strangeman
 */

#ifndef NUMERALS_HPP_
#define NUMERALS_HPP_

#include "char_vector.hpp"

namespace Numerals {

unsigned int SimpleAbs ( int _num );

void SmallNumsToCharVector( int _num, CharVector::ChVector & _dest, bool _getOrdinal = false );

void NumeralToCharVector( int _num, CharVector::ChVector & _dest, bool _getOrdinal = false );

const char * GetPtrToFirstDigit( const char * _cString );

} //End of Numerals namespace

#endif /* NUMERALS_HPP_ */
