/*
 * support_functions.hpp
 *
 *  Created on: 8 июня 2015
 *      Author: strangeman
 */

#ifndef SUPPORT_FUNCTIONS_HPP_
#define SUPPORT_FUNCTIONS_HPP_

#include "char_vector.hpp"
#include "date_lib.hpp"
#include <iostream>

void PrintMessage( Dates::ExtractStatus _status, std::ostream & _messageStream = std::cout );

void GetEndianFromUser( std::istream & _stream, Dates::DateEndian & _endian, std::ostream & _messageStream = std::cout );

bool GetDateFromUser( std::istream & _stream, Dates::Date & _targetDate, Dates::DateEndian & _endian, std::ostream & _messageStream = std::cout );

#endif /* SUPPORT_FUNCTIONS_HPP_ */
