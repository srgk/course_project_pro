/*
 * date_lib.cpp
 *
 *  Created on: 5 июня 2015
 *      Author: strangeman
 */

#include <cstdlib>
#include <cstring>
#include <cassert>
#include <cctype>

#include "date_lib.hpp"
#include "char_vector.hpp"
#include "numerals.hpp"

namespace Dates {

enum Month {
	JANUARY = 1, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER
};

const char * g_monthNames[] = {
	"Error", "January", "February", "March", "April", "May", "June",
	"July", "August", "September", "October", "November", "December"
};

const int g_maxDaysInMonth[13] = {
	0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

const char * g_daysOfWeek[] = {
	"Error", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
};

bool IsLeapYear( const int _year ) {
	if ((_year % 4) == 0)
		if (((_year % 100) > 0) || ((_year % 400) == 0))
			return true;

	//else do this:
	return false;
}

int GetMaxDaysInMonth( const Date & _date ) {
	Month currMonth = static_cast<Month>(_date.m_month);
	int max = g_maxDaysInMonth[ currMonth ];
	if (( currMonth == FEBRUARY ) && IsLeapYear( _date.m_year ))
		max++;

	return max;
}

void DateToTM( Date & _dateOrig, tm   & _dateNew ) {
	_dateNew.tm_mday = _dateOrig.m_day;
	_dateNew.tm_mon = _dateOrig.m_month;
	_dateNew.tm_year = _dateOrig.m_year;
}

// Много if-ов - выглядит страшно
DateEndian GetEndian( const int * _numArray, const int _nOfNums ) {
	assert( _nOfNums == 3 );

	// Для начала проверим корректность данных
	if( ( _numArray[0] < 0 ) || ( _numArray[1] < 0 ) || ( _numArray[2] < 0 ) )
		return UNDEF;

	const unsigned short maxDayInAnyMonth = 31, maxMonthInYear = 12;

	/*
	 * По умолчанию считаем, что пришла дата в Little-endian
	 * Тогда первым числом должен стоять день
	 */
	if( _numArray[0] <= maxDayInAnyMonth ) {
		// Вторым числом должен быть месяц
		if( _numArray[1] <= maxMonthInYear )
			return LITTLE;

		// Если одно из условий не выполнилось - ищем дальше
	}

	/*
	 * Попробуем применить Middle-endian
	 * Тогда первым должен стоять месяц
	 */
	if( _numArray[0] <= maxMonthInYear ) {
		// Вторым - день
		if( _numArray[1] <= maxDayInAnyMonth )
			return MIDDLE;

		// Если какое-то условие не выполнилось - ищем дальше
	}

	/*
	 * Остался только Big-endian, проверяем
	 * Второе число тогда должно быть месяцем
	 */
	if( _numArray[1] <= maxMonthInYear )
		// А второе - днём
		if( _numArray[2] <= maxDayInAnyMonth )
			return BIG;

	// Иначе - нам дали неправильную дату
	return UNDEF;
}

bool IsDateValid( const Date & _date ) {
	if( _date.m_month > 12 ) {
		return false;
	}

	if( _date.m_day > GetMaxDaysInMonth( _date ) )
		return false;

	if( _date.m_day <= 0 )
		return false;

	if( _date.m_month <= 0 )
		return false;

	if( _date.m_year == 0 )
		return false;

	return true;
}

// Принимает на вход массив из трёх чисел и пытается из них сформировать дату
ExtractStatus FormDate( const int * _numArray, const int _nOfNums, Date & _date, DateEndian _endian ) {

	assert( _nOfNums == 3 );

	if( _endian == UNDEF )
		_endian = GetEndian( _numArray, _nOfNums );

	if( _endian == UNDEF )
		return BAD_DATE;

	switch( _endian ) {
	case LITTLE:
		_date.m_day = _numArray[ 0 ];
		_date.m_month = _numArray[ 1 ];
		_date.m_year = _numArray[ 2 ];
		break;

	case MIDDLE:
		_date.m_day = _numArray[ 1 ];
		_date.m_month = _numArray[ 0 ];
		_date.m_year = _numArray[ 2 ];
		break;

	case BIG:
		_date.m_day = _numArray[ 2 ];
		_date.m_month = _numArray[ 1 ];
		_date.m_year = _numArray[ 0 ];
		break;

	default:
		assert( ! "You mustn't be here" );
	}

	if( ! IsDateValid( _date ) )
		return BAD_DATE;

	return OK;

}

const char * GetPtrToFirstDigit( const char * _cString ) {
	int currStrLen = strlen( _cString );
	for( int i = 0; i < currStrLen; i++ ) {
		if( isdigit( _cString[ i ] ) ) {
			return &_cString[ i ];
		}
	}

	return nullptr;
}

char GetFirstSeparator( const char * _cString, const char * _dateSeparators ) {
	for( int i = 0; _cString[ i ]; i++ )
		for( int j = 0; _dateSeparators[ j ]; j++ )
			if (_dateSeparators[ j ] == _cString[ i ] )
				return _cString[ i ];

	return '\0';
}

int CountSeparators( const char * _cString, const char * _dateSeparators ) {
	int counter = 0;

	for( int i = 0; _cString[ i ]; i++ )
		for( int j = 0; _dateSeparators[ j ]; j++ )
			if (_dateSeparators[ j ] == _cString[ i ] ) {
				counter ++;
				break;
			}

	return counter;
}

/*
 * Нужно получить три числа - день, месяц и год.
 * Они могут следовать в любом порядке и разделяться любыми символами.
 * Задача: обработать входную строку и извлечь из нее дату
 */
ExtractStatus ExtractDate( const char * _input, Date & _date, DateEndian _endian, const char * _dateSeparators ) {
	ExtractStatus status = OK;
	int inputStrSize = strlen( _input ) + 1;

	// Если на вход пришла пустая строка - выходим сразу
	if( inputStrSize == 1 )
		return BAD_INPUT_FORMAT;

	// Копируем входные данные во временный массив
	char * inputCopy = new char [ inputStrSize ];
	strcpy( inputCopy, _input );

	// Объявляем рабочие указатели
	char * currPtr;
	char * firstCharAfterNumberPtr;
	const char * firstDigitPtr;

	// Объявляем строку, в которой будет содержаться выбранный разделитель
	const char currentSeparator[ 2 ] = {
			GetFirstSeparator( _input, _dateSeparators ), '\0'
	};

	if( CountSeparators( _input, currentSeparator) > 0 ) {
		// Если разделителей во вводе больше, чем нужно - уже можно ругаться
		if( CountSeparators( _input, currentSeparator) > 2 )
			status = NOT_ONLY_NUMBERS;
	}
	else {
		// Если разделителей не найдено вообще - выходим
		delete [] inputCopy;
		return BAD_INPUT_FORMAT;
	}

	// Временный массив для хранения трех извлеченных из ввода чисел
	const int nOfTempNum = 3;
	int tempNums[ nOfTempNum ];

	// Разделяем строку на токены
	strtok( inputCopy, currentSeparator );

	// Считаем извлеченные числа
	int numCounter = 0;

	// Начинаем с начала строки
	currPtr = inputCopy;

	// Цикл для получения чисел
	while( currPtr ) {
		//Обнуляем рабочие указатели
		firstCharAfterNumberPtr = nullptr;
		firstDigitPtr = nullptr;

		/*
		 * Если мы уже получили три числа, но все еще
		 * не вышли из цикла (в входной строке
		 * остались токены) - ругаемся и выходим из цикла
		 */
		if( numCounter >= nOfTempNum ) {
			status = TOO_MUCH_NUMBERS;
			break;
		}
		// Иначе - продолжаем анализ
		else {
			// Находим указатель на первое число
			firstDigitPtr = GetPtrToFirstDigit( currPtr );

			// Если число вообще существует в токене
			if( firstDigitPtr != nullptr) {
				// Проверяем, не стоят ли перед ним какие-то другие символы
				if( firstDigitPtr != currPtr)
					status = NOT_ONLY_NUMBERS;

				// И пытаемся извлечь из токена это самое число
				tempNums[ numCounter++ ] = Numerals::SimpleAbs( strtol( firstDigitPtr, &firstCharAfterNumberPtr, 10 ) );
			}
		}

		// Проверяем на переполнение int

		// Получаем количество символов в цифре
		int nOfDigits = firstCharAfterNumberPtr - firstDigitPtr;


		if( nOfDigits > 0 ) {
			// Если число представлено больше, чем 10-ю символами:
			if( nOfDigits > 10 ) {
				// Ругаемся и выходим
				status = BAD_DATE;
				break;
			}

			// Или если число больше 2-х миллиардов:
			if( nOfDigits == 10 ) {
				// Ругаемся и выходим
				if( ( firstDigitPtr[ 0 ] > '2' ) ) {
					status = BAD_DATE;
					break;
				}

				else if ( ( firstDigitPtr[ 0 ] == '2' )
							&&
							( firstDigitPtr[ 1 ] != '0' ) ) {

					status = BAD_DATE;
					break;
				}
			}
		}


		// Опять проверяем на лишние символы
		if( status != NOT_ONLY_NUMBERS ) {
			// Если strtol нашел какие-то числа, обращаемся к следующему за числом символу...
			if( firstCharAfterNumberPtr ) {
				// ...проверяем, что это - завершающий ноль, который закрывает токен
				if ( firstCharAfterNumberPtr[0] != '\0' )
					status = NOT_ONLY_NUMBERS;
			}
		}

		// Выделяем новый токен
		currPtr = strtok( NULL, currentSeparator );

	}

	if( numCounter < nOfTempNum )
		status = BAD_INPUT_FORMAT;
	else
		if ( FormDate( tempNums, nOfTempNum, _date, _endian ) == BAD_DATE )
			status = BAD_DATE;

	delete [] inputCopy;
	return status;
}

ExtractStatus ExtractDate( const char * _input, tm   & _date, DateEndian _endian, const char * _dateSeparators ) {
	Date tempDate;
	ExtractStatus extractStatus;
	extractStatus = ExtractDate( _input, tempDate, _endian, _dateSeparators );

	DateToTM( tempDate, _date );

	return extractStatus;
}

// Example: The fifteenth of May, nineteen forty-eight
void   GetTextDate( const Date & _date, CharVector::ChVector & _string ) {

	const int day = _date.m_day;
	const int month = _date.m_month;
	const int year = _date.m_year;

	CharVector::Clear( _string );

	CharVector::Concatenate( _string, "The " );
	Numerals::SmallNumsToCharVector( day, _string, true );
	CharVector::Concatenate( _string, " of " );
	CharVector::Concatenate( _string, g_monthNames[ month ] );
	CharVector::Concatenate( _string, ", year " );
	if( (year / 10000) || ! ( year % 100 ) ) {
		Numerals::NumeralToCharVector( year, _string, false );
	}
	else {
		if( year / 1000) {
			Numerals::SmallNumsToCharVector( year / 100 , _string, false );
			CharVector::Concatenate( _string, " " );
			Numerals::SmallNumsToCharVector( year % 100 , _string, false );
		}
		else {
			Numerals::SmallNumsToCharVector( year, _string, false );
		}
	}


}

char * GetTextDate( const Date & _date ) {
	CharVector::ChVector result;
	CharVector::Init( result );
	GetTextDate( _date, result );
	return result.m_pData;
}

}// end of Dates
